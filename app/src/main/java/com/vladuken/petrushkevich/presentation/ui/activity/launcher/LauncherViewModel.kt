package com.vladuken.petrushkevich.presentation.ui.activity.launcher

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.vladuken.petrushkevich.data.usecase.ReadAppsUseCase
import com.vladuken.petrushkevich.domain.entity.App
import com.vladuken.petrushkevich.presentation.ui.core.repository.LauncherLayoutRepository
import kotlinx.coroutines.launch

class LauncherViewModel(
    private val launcherLayoutRepository: LauncherLayoutRepository,
    private val readAppsUseCase: ReadAppsUseCase,
    app: Application
) : AndroidViewModel(app) {

    val apps = MutableLiveData<List<App>>()

    init {
        viewModelScope.launch {
            val fetchedApps = readAppsUseCase.readApps()
            apps.postValue(fetchedApps)
        }
    }

    val launcherLayout get() = launcherLayoutRepository.currentLayout
}
