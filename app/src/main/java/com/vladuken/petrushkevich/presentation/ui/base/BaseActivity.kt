package com.vladuken.petrushkevich.presentation.ui.base

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import org.koin.androidx.viewmodel.ext.android.viewModel

abstract class BaseActivity : AppCompatActivity() {
    protected val viewModel: BaseViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(viewModel.theme.id)
        super.onCreate(savedInstanceState)

        viewModel.reloadActivityEvent.observe(this, Observer {
            if (it) {
                viewModel.reloadActivityEvent.postValue(false)
                restartActivity()
            }
        })
    }

    open fun restartActivity() = recreate()

    protected fun finishAndStartActivity(
        classType: Class<*>,
        flags: Int = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    ) {
        Intent(this, classType).run {
            this.flags = flags
            startActivity(this)
            finish()
        }
    }
}
