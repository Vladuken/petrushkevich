package com.vladuken.petrushkevich.presentation.ui.activity.welcome

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.vladuken.petrushkevich.R
import com.vladuken.petrushkevich.presentation.ui.base.BaseViewModel
import com.vladuken.petrushkevich.presentation.ui.core.repository.LauncherLayoutRepository.LauncherLayout
import com.vladuken.petrushkevich.presentation.ui.core.repository.ThemeRepository.Theme
import kotlinx.android.synthetic.main.fragment_choose_launcher_layout.view.*
import kotlinx.android.synthetic.main.fragment_choose_theme.view.*
import kotlinx.android.synthetic.main.fragment_round_image_with_description.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

sealed class WelcomeFragments : Fragment()

class RoundImageWithDescriptionFragment : WelcomeFragments() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_round_image_with_description, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.run {
            ivRoundImage.setImageResource(getInt(KEY_IMAGE_ID))
            tvTitle.text = getText(getInt(KEY_TITLE_ID))
            tvSubtitle.text = getText(getInt(KEY_SUBTITLE_ID))
        }
    }

    companion object {

        private const val KEY_IMAGE_ID = "KEY_IMAGE_ID"
        private const val KEY_TITLE_ID = "KEY_TITLE_ID"
        private const val KEY_SUBTITLE_ID = "KEY_SUBTITLE_ID"

        fun newInstance(
            imageId: Int,
            titleId: Int,
            subtitleId: Int
        ): RoundImageWithDescriptionFragment {
            return RoundImageWithDescriptionFragment().apply {
                arguments = Bundle().apply {
                    putInt(KEY_IMAGE_ID, imageId)
                    putInt(KEY_TITLE_ID, titleId)
                    putInt(KEY_SUBTITLE_ID, subtitleId)
                }
            }
        }
    }
}

class SimpleLayoutFragment : WelcomeFragments() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(arguments.layoutId, container, false)
    }

    companion object {

        private const val KEY_LAYOUT_ID = "KEY_LAYOUT_ID"

        private var Bundle?.layoutId: Int
            set(value) {
                this?.putInt(KEY_LAYOUT_ID, value)
            }
            get() {
                return this?.getInt(KEY_LAYOUT_ID)
                    ?: throw NoSuchElementException("You should provide layoutIdl")
            }

        fun newInstance(layoutId: Int): SimpleLayoutFragment {
            return SimpleLayoutFragment().apply {
                arguments = Bundle().apply {
                    this.layoutId = layoutId
                }
            }
        }
    }
}

class ThemeChooseFragment : WelcomeFragments() {

    private val viewModel: BaseViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_choose_theme, container, false)

        with(view) {
            when (viewModel.theme) {
                Theme.DARK -> rbDarkTheme
                Theme.LIGHT -> rbLightTheme
            }.isChecked = true

            rbDarkTheme.setOnClickListener { viewModel.theme = Theme.DARK }
            rbLightTheme.setOnClickListener { viewModel.theme = Theme.LIGHT }
        }

        return view
    }

    companion object {
        fun newInstance(): ThemeChooseFragment {
            return ThemeChooseFragment()
        }
    }
}

class LauncherLayoutChooseFragment : WelcomeFragments() {

    private val viewModel: BaseViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_choose_launcher_layout, container, false)

        with(view) {
            when (viewModel.activityLayout) {
                LauncherLayout.DENSE -> rbDenseLayout
                LauncherLayout.STANDARD -> rbStandardLayout
            }.isChecked = true

            rbStandardLayout.setOnClickListener {
                viewModel.activityLayout = LauncherLayout.STANDARD
            }
            rbDenseLayout.setOnClickListener { viewModel.activityLayout = LauncherLayout.DENSE }
        }

        return view
    }

    companion object {
        fun newInstance(): LauncherLayoutChooseFragment {
            return LauncherLayoutChooseFragment()
        }
    }
}
