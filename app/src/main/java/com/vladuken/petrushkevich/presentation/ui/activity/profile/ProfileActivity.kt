package com.vladuken.petrushkevich.presentation.ui.activity.profile

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.vladuken.petrushkevich.R
import com.vladuken.petrushkevich.presentation.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_profile_page.*
import kotlinx.android.synthetic.main.include_profile_page.*

class ProfileActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(
            R.anim.nav_default_pop_enter_anim,
            R.anim.nav_default_pop_exit_anim
        )
        setContentView(R.layout.activity_profile_page)
        supportActionBar?.hide()
        profileToolbar.setNavigationOnClickListener { onBackPressed() }

        rvPhoneNumber.onClickStartIntent(R.id.tvPhoneNumber, Intent.ACTION_DIAL, "tel:")
        rvEmail.onClickStartIntent(R.id.tvEmail, Intent.ACTION_SENDTO, "mailto:")
        rvInstagram.onClickStartIntent(R.id.tvInstagram, Intent.ACTION_VIEW, "http:")
        rvGithub.onClickStartIntent(R.id.tvGithub, Intent.ACTION_VIEW, "http:")

        rvCardNumber.setOnClickListener {
            with(getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager) {
                primaryClip = ClipData.newPlainText("", tvCardNumber.text.toString())
            }

            Snackbar.make(it, getString(R.string.copied), Snackbar.LENGTH_SHORT)
                .setAction(getString(R.string.dismiss)) {}
                .show()
        }

        fab.setOnClickListener {
            runIntent(Intent.ACTION_VIEW, "http:${getString(R.string.author_telegram)}")
        }
    }

    private fun View.onClickStartIntent(
        textViewId: Int,
        intentAction: String,
        uriPrefix: String
    ) = setOnClickListener {
        val s = findViewById<TextView>(textViewId).text.toString()
        runIntent(intentAction, uriPrefix + s)
    }

    private fun runIntent(intentAction: String, resultString: String) {
        Intent(intentAction).apply {
            data = Uri.parse(resultString)
            startActivity(this)
        }
    }
}
