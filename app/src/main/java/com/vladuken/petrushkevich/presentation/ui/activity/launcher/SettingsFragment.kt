package com.vladuken.petrushkevich.presentation.ui.activity.launcher

import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.vladuken.petrushkevich.R

class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.app_preferences, rootKey)

        // TODO maybe add keys to string array
        listOf(
            R.string.preference_key_launcher_layout,
            R.string.preference_key_theme
        )
            .map(::getString)
            .mapNotNull { findPreference<Preference>(it) }
            .forEach(::setOnPreferenceChangeListener)
    }

    private fun setOnPreferenceChangeListener(preference: Preference) {
        preference.setOnPreferenceChangeListener { _, _ ->
            // TODO delegate recreation to activity
            activity?.recreate()
            true
        }
    }

    companion object {
        fun newInstance(): SettingsFragment {
            return SettingsFragment()
        }
    }
}
