package com.vladuken.petrushkevich.presentation.ui.core.repository

import android.content.Context
import android.content.SharedPreferences
import com.vladuken.petrushkevich.R

class ThemeRepository(private val preference: SharedPreferences, context: Context) {

    enum class Theme(val id: Int) {
        DARK(R.style.AppTheme_Dark),
        LIGHT(R.style.AppTheme_Light)
    }

    private val key = context.getString(R.string.preference_key_theme)

    // TODO CHANGE THEME TO NUMBER
    private val lightTheme: String = context.getString(R.string.light_theme)
    private val darkTheme: String = context.getString(R.string.dark_theme)

    var currentTheme: Theme
        get() {
            return when (preference.getString(key, lightTheme)) {
                lightTheme -> Theme.LIGHT
                darkTheme -> Theme.DARK
                else -> Theme.LIGHT
            }
        }
        set(value) {
            val themeString = when (value) {
                Theme.DARK -> darkTheme
                Theme.LIGHT -> lightTheme
            }

            with(preference.edit()) {
                putString(key, themeString)
                apply()
            }
        }
}
