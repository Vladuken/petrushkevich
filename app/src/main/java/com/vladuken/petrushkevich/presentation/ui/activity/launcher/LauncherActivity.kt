package com.vladuken.petrushkevich.presentation.ui.activity.launcher

import android.content.Intent
import android.os.Bundle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabLayoutMediator
import com.vladuken.petrushkevich.R
import com.vladuken.petrushkevich.presentation.ui.activity.profile.ProfileActivity
import com.vladuken.petrushkevich.presentation.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_launcher.*
import kotlinx.android.synthetic.main.activity_launcher_nav_drawer.*

class LauncherActivity : BaseActivity() {

    private val pagerAdapter = LauncherPagerAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher_nav_drawer)
        initViews()
    }

    private fun initViews() {
        with(vpScreens) {
            adapter = pagerAdapter
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)

                    with(navViewLauncher) {
                        uncheckAllMenuItems()
                        menu.getItem(position).isChecked = true
                    }

                    supportActionBar?.setDisplayHomeAsUpEnabled(position != 0)
                }
            })
        }

        TabLayoutMediator(tabDots, vpScreens) { _, _ -> }.attach()

        with(navViewLauncher) {
            initNavigationView(navDrawer)
            getHeaderView(0).setOnClickListener {
                Intent(context, ProfileActivity::class.java).let(::startActivity)
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        vpScreens.currentItem = 0
        return true
    }

    private fun NavigationView.initNavigationView(drawer: DrawerLayout) {
        setupMenu()

        setNavigationItemSelectedListener { menuItem ->
            // TODO refactor and make better solution
            uncheckAllMenuItems()
            menuItem.isChecked = true
            vpScreens.currentItem = menuItem.itemId
            drawer.closeDrawers()
            true
        }
    }

    // TODO move to ext class
    private fun NavigationView.setupMenu() {
        for (position in 0 until pagerAdapter.itemCount) {
            val viewType = pagerAdapter.getItemViewType(position)
            val type = LauncherPagerAdapter.PageType.values[viewType]
            menu.add(0, position, 0, getString(type.titleRes))
        }

        invalidate()
    }

    // TODO move to ext class
    private fun NavigationView.uncheckAllMenuItems() {
        val size = menu.size()
        for (i in 0 until size) {
            menu.getItem(i).isChecked = false
        }
    }
}
