package com.vladuken.petrushkevich.presentation.di

import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import com.vladuken.petrushkevich.R
import com.vladuken.petrushkevich.data.usecase.ReadAppsUseCase
import com.vladuken.petrushkevich.presentation.ui.activity.launcher.LauncherViewModel
import com.vladuken.petrushkevich.presentation.ui.base.BaseViewModel
import com.vladuken.petrushkevich.presentation.ui.core.repository.FirstLaunchRepository
import com.vladuken.petrushkevich.presentation.ui.core.repository.LauncherLayoutRepository
import com.vladuken.petrushkevich.presentation.ui.core.repository.ThemeRepository
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private val viewModelModule = module {
    single { BaseViewModel(get(), get(), get()) }
    viewModel { LauncherViewModel(get(), get(), get()) }
}

private val repositoryModule = module {
    single { ThemeRepository(get(), androidContext()) }
    single { LauncherLayoutRepository(get(), androidContext()) }
    single { FirstLaunchRepository(get(), androidContext()) }
}
private val useCases = module {
    factory { ReadAppsUseCase(get()) }
}

private val dataModule = module {
    single<SharedPreferences> {
        androidContext().getSharedPreferences(
            androidContext().getString(R.string.preference_file),
            Context.MODE_PRIVATE
        )
    }
    single<PackageManager> {
        androidContext().packageManager
    }
}

val modules = listOf(viewModelModule, repositoryModule, dataModule, useCases)
