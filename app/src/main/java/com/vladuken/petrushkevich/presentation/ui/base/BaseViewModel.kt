package com.vladuken.petrushkevich.presentation.ui.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vladuken.petrushkevich.presentation.ui.core.repository.FirstLaunchRepository
import com.vladuken.petrushkevich.presentation.ui.core.repository.LauncherLayoutRepository
import com.vladuken.petrushkevich.presentation.ui.core.repository.ThemeRepository

class BaseViewModel(
    private val themeRepository: ThemeRepository,
    private val launcherLayoutRepository: LauncherLayoutRepository,
    private val firstLaunchRepository: FirstLaunchRepository
) : ViewModel() {

    val reloadActivityEvent = MutableLiveData(false)

    // TODO read about delegated properties and maybe change them
    var theme
        get() = themeRepository.currentTheme
        set(value) {
            themeRepository.currentTheme = value
            reloadActivityEvent.postValue(true)
        }

    var activityLayout
        get() = launcherLayoutRepository.currentLayout
        set(value) {
            launcherLayoutRepository.currentLayout = value
        }

    var isFirstLaunch
        get() = firstLaunchRepository.isFirstLaunch
        set(value) {
            firstLaunchRepository.isFirstLaunch = value
        }
}
