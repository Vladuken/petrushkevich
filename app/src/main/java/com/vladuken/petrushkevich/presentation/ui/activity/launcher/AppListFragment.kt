package com.vladuken.petrushkevich.presentation.ui.activity.launcher

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.vladuken.petrushkevich.R
import com.vladuken.petrushkevich.presentation.ui.activity.launcher.AppListFragment.LayoutType.GRID
import com.vladuken.petrushkevich.presentation.ui.activity.launcher.AppListFragment.LayoutType.LINEAR
import kotlinx.android.synthetic.main.fragment_app_list.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class AppListFragment : Fragment() {

    private val viewModel: LauncherViewModel by sharedViewModel()

    private lateinit var appsAdapter: AppListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_app_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()

        viewModel.apps.observe(viewLifecycleOwner, Observer {
            appsAdapter.submitList(it)
        })
    }

    private fun initViews() {

        val (layoutManager, itemLayoutId) = when (arguments.layoutLayoutType) {
            LINEAR -> {
                LinearLayoutManager(requireContext()) to R.layout.item_app_icon_linear
            }
            GRID -> {
                GridLayoutManager(requireContext(), gridRowCount) to R.layout.item_app_icon_grid
            }
        }

        appsAdapter = AppListAdapter(itemLayoutId)

        with(rvApps) {
            this.layoutManager = layoutManager
            adapter = appsAdapter
        }
    }

    private val gridRowCount: Int
        get() = if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            viewModel.launcherLayout.landscapeWidth
        } else {
            viewModel.launcherLayout.portraitWidth
        }

    enum class LayoutType {
        LINEAR,
        GRID
    }

    companion object {

        private const val KEY_TYPE = "KEY_TYPE"

        /* Extensions */
        var Bundle?.layoutLayoutType: LayoutType
            set(value) {
                this?.putSerializable(KEY_TYPE, value)
            }
            get() {
                return this?.getSerializable(KEY_TYPE) as? LayoutType
                    ?: throw IllegalArgumentException("You should provide type")
            }

        fun newInstance(layoutType: LayoutType): AppListFragment {
            val arguments = Bundle().apply {
                this.layoutLayoutType = layoutType
            }

            return AppListFragment().also {
                it.arguments = arguments
            }
        }
    }
}
