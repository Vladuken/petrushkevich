package com.vladuken.petrushkevich.presentation.app

import android.app.Application
import com.vladuken.petrushkevich.presentation.di.modules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@BaseApplication)
            modules(modules)
        }
    }
}
