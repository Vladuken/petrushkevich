package com.vladuken.petrushkevich.presentation.ui.core.repository

import android.content.Context
import android.content.SharedPreferences
import com.vladuken.petrushkevich.R

class LauncherLayoutRepository(private val preference: SharedPreferences, context: Context) {

    companion object {
        private const val STANDARD_PORTRAIT_WIDTH = 4
        private const val STANDARD_LANDSCAPE_WIDTH = 6
        private const val DENSE_PORTRAIT_WIDTH = 5
        private const val DENSE_LANDSCAPE_WIDTH = 7
    }

    enum class LauncherLayout(val portraitWidth: Int, val landscapeWidth: Int) {
        STANDARD(STANDARD_PORTRAIT_WIDTH, STANDARD_LANDSCAPE_WIDTH),
        DENSE(DENSE_PORTRAIT_WIDTH, DENSE_LANDSCAPE_WIDTH)
    }

    private val key = context.getString(R.string.preference_key_launcher_layout)

    // TODO CHANGE STRINGS FOR NUMBERS
    private val standardLayout: String = context.getString(R.string.standard_launcher_layout)
    private val denseLayout: String = context.getString(R.string.dense_launcher_layout)

    var currentLayout: LauncherLayout
        get() {
            return when (preference.getString(key, standardLayout)) {
                standardLayout -> LauncherLayout.STANDARD
                denseLayout -> LauncherLayout.DENSE
                else -> LauncherLayout.STANDARD
            }
        }
        set(value) {
            val launcherLayout = when (value) {
                LauncherLayout.STANDARD -> standardLayout
                LauncherLayout.DENSE -> denseLayout
            }

            with(preference.edit()) {
                putString(key, launcherLayout)
                apply()
            }
        }
}
