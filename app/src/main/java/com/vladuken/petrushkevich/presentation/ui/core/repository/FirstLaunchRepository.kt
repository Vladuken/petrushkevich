package com.vladuken.petrushkevich.presentation.ui.core.repository

import android.content.Context
import android.content.SharedPreferences
import com.vladuken.petrushkevich.R

class FirstLaunchRepository(
    private val preference: SharedPreferences,
    context: Context
) {

    private val key = context.getString(R.string.preference_key_first_launch)

    var isFirstLaunch: Boolean
        get() = preference.getBoolean(key, true)
        set(value) {
            with(preference.edit()) {
                putBoolean(key, value)
                apply()
            }
        }
}
