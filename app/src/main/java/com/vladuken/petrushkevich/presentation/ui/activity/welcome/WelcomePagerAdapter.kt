package com.vladuken.petrushkevich.presentation.ui.activity.welcome

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.vladuken.petrushkevich.R

class WelcomePagerAdapter(fa: FragmentActivity) :
    FragmentStateAdapter(fa) {

    companion object {
        const val COUNT_PAGES = 4
        private const val APP_FRAGMENT_ID = 0
        private const val AUTHOR_FRAGMENT_ID = 1
        private const val THEME_FRAGMENT_ID = 2
        private const val LAYOUT_FRAGMENT_ID = 3
    }

    override fun getItemCount(): Int = COUNT_PAGES

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            APP_FRAGMENT_ID -> RoundImageWithDescriptionFragment.newInstance(
                R.mipmap.ic_launcher,
                R.string.app_name,
                R.string.app_description
            )
            AUTHOR_FRAGMENT_ID -> RoundImageWithDescriptionFragment.newInstance(
                R.drawable.photo_author,
                R.string.author_name,
                R.string.author_description
            )
            THEME_FRAGMENT_ID -> ThemeChooseFragment.newInstance()
            LAYOUT_FRAGMENT_ID -> LauncherLayoutChooseFragment.newInstance()

            else -> throw NoSuchElementException("There are no such fragment")
        }
    }
}
