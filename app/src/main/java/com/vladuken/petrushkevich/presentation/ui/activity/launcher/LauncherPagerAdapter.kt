package com.vladuken.petrushkevich.presentation.ui.activity.launcher

import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.vladuken.petrushkevich.R

class LauncherPagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

    companion object {
        const val COUNT_PAGES = 3
    }

    override fun getItemCount(): Int = COUNT_PAGES

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> AppListFragment.newInstance(AppListFragment.LayoutType.GRID)
            1 -> AppListFragment.newInstance(AppListFragment.LayoutType.LINEAR)
            2 -> SettingsFragment.newInstance()
            else -> throw NoSuchElementException("There are no such fragment")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0, 1, 2 -> PageType.values[position].ordinal
            else -> super.getItemViewType(position)
        }
    }

    enum class PageType(@StringRes val titleRes: Int) {
        GRID(R.string.grid),
        LINEAR(R.string.list),
        SETTINGS(R.string.settings),
        DESKTOP(R.string.next);

        companion object {
            val values: Array<PageType> = values()
        }
    }
}
