package com.vladuken.petrushkevich.presentation.ui.activity.welcome

import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.vladuken.petrushkevich.R
import com.vladuken.petrushkevich.presentation.ui.activity.launcher.LauncherActivity
import com.vladuken.petrushkevich.presentation.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : BaseActivity() {

    private val welcomePagerAdapter = WelcomePagerAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_welcome)
        initViews()
    }

    private fun initViews() {
        btnNext.setOnClickListener {
            if (vpSlides.currentItem < welcomePagerAdapter.itemCount - 1) {
                vpSlides.currentItem++
            } else {
                viewModel.isFirstLaunch = false
                finishAndStartActivity(LauncherActivity::class.java)
            }
        }

        with(vpSlides) {
            adapter = welcomePagerAdapter
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)

                    val stringRes = if (position == welcomePagerAdapter.itemCount - 1) {
                        R.string.start
                    } else {
                        R.string.next
                    }

                    btnNext.setText(stringRes)
                }
            })
        }

        TabLayoutMediator(tabDots, vpSlides) { _, _ -> }.attach()
    }

    override fun onBackPressed() {
        if (vpSlides.currentItem == 0) {
            super.onBackPressed()
        } else {
            vpSlides.currentItem--
        }
    }
}
