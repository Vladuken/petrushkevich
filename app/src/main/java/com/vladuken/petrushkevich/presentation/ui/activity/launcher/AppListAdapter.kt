package com.vladuken.petrushkevich.presentation.ui.activity.launcher

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.vladuken.petrushkevich.domain.entity.App
import kotlinx.android.synthetic.main.item_app_icon_grid.view.*

class AppListAdapter(@LayoutRes private val layoutId: Int) :
    ListAdapter<App, AppListAdapter.ViewHolder>(AppCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(app: App) {
            with(itemView) {
                ivAppIcon.background = app.icon
                tvAppTitle.text = app.packageName

                setOnClickListener {
                    val intent = it.context.packageManager.getLaunchIntentForPackage(app.packageName)
                    it.context.startActivity(intent)
                }
            }
        }
    }

    private class AppCallback : DiffUtil.ItemCallback<App>() {
        override fun areItemsTheSame(oldItem: App, newItem: App): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: App, newItem: App): Boolean {
            return oldItem.packageName == newItem.packageName
        }
    }
}
