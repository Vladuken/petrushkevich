package com.vladuken.petrushkevich.data.usecase

import android.content.Intent
import android.content.pm.PackageManager
import com.vladuken.petrushkevich.domain.entity.App

class ReadAppsUseCase(private val pm: PackageManager) {

    fun readApps(): List<App> {
        val startupIntent = Intent(Intent.ACTION_MAIN)
        startupIntent.addCategory(Intent.CATEGORY_LAUNCHER)
        val a = pm.getInstalledApplications(0)

        return a.mapNotNull {
            App(
                it.packageName,
                pm.getApplicationLabel(it).toString(),
                it.loadIcon(pm)
            )
        }
    }
}
