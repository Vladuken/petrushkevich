package com.vladuken.petrushkevich

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView

class RoundImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    private val path = Path()
    private val rect = RectF()

    override fun onDraw(canvas: Canvas?) {
        val radius = this.height / 2.toFloat()
        rect.set(0f, 0f, width.toFloat(), height.toFloat())

        path.addRoundRect(rect, radius, radius, Path.Direction.CW)
        canvas!!.clipPath(path)
        super.onDraw(canvas)
    }
}
