package com.vladuken.petrushkevich

import android.os.Bundle
import com.vladuken.petrushkevich.presentation.ui.activity.launcher.LauncherActivity
import com.vladuken.petrushkevich.presentation.ui.activity.welcome.WelcomeActivity
import com.vladuken.petrushkevich.presentation.ui.base.BaseActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (viewModel.isFirstLaunch) {
            WelcomeActivity::class.java
        } else {
            LauncherActivity::class.java
        }.let { finishAndStartActivity(it) }
    }
}
